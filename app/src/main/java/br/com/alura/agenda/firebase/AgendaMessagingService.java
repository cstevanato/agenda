package br.com.alura.agenda.firebase;

import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.Map;

import br.com.alura.agenda.dto.AlunoSync;
import br.com.alura.agenda.event.AtualizarListaAlunoEvent;
import br.com.alura.agenda.sinc.AlunoSincronizador;

public class AgendaMessagingService extends FirebaseMessagingService {

    private static final String TAG = "FCM Service";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        //
        //TODO: Handle FCM messages here.
        //If the application is in the foreground handle both data and notification messages here.
        //Also if you intend on generating your own notifications as a result of a received FCM
        //message, here is where that should be initiated.
        //

        Map<String, String> data = remoteMessage.getData();

        converteParaAluno(data);

        Log.i("mesagem Recebida", "mesagem: " + String.valueOf(data));
//        Log.d(TAG, "From: " + remoteMessage.getData());
//        Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
    }

    private void converteParaAluno(Map<String, String> data) {
        String chaveAcesso = "alunoSync";
        if (data.containsKey(chaveAcesso)) {
            String json = data.get(chaveAcesso);
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                AlunoSync alunoSync = objectMapper.readValue(json, AlunoSync.class);

                new AlunoSincronizador(AgendaMessagingService.this).sincroniza(alunoSync);

                EventBus aDefault = EventBus.getDefault();
                aDefault.post(new AtualizarListaAlunoEvent());

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
