package br.com.alura.agenda.dto;

import java.io.Serializable;
import java.util.List;

import br.com.alura.agenda.modelo.Aluno;

public class AlunoSync implements Serializable {

    public List<Aluno> alunos;
    public String momentoDaUltimaModificacao;

    public List<Aluno> getAlunos() {
        return alunos;
    }

    public String getMonentoDaUltimaMotificacao() {
        return momentoDaUltimaModificacao;
    }
}
