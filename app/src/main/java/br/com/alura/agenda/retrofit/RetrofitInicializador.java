package br.com.alura.agenda.retrofit;

import br.com.alura.agenda.services.AlunoService;
import br.com.alura.agenda.services.DespositivoService;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class RetrofitInicializador {


    //private final String endereco = "http://10.220.236.105:8080/api/";
    //private final String endereco = "http://192.168.0.101:8080/api/";
    private final String endereco = "http://192.168.0.104:8080/api/";
    private final Retrofit retrofit;
    public  RetrofitInicializador() {

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.addInterceptor(loggingInterceptor);

       retrofit = new Retrofit.Builder().baseUrl(endereco)
                .client(client.build())
                .addConverterFactory(JacksonConverterFactory.create()).build();
    }

    public AlunoService getAlunoService() {
        return retrofit.create(AlunoService.class);
    }

    public DespositivoService getDispositivoService() {
        return retrofit.create(DespositivoService.class);
    }
}
